The *Containerfile* file is this directory was initially used in combination with *podman* as a container engine.
It is no longer used, but is left here, as it sets the ground for future customization.
