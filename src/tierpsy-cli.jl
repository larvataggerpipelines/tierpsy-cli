
using ArgParse
using OrderedCollections

include("TierpsyWrapper.jl")
using .TierpsyWrapper

function main(args=ARGS)
    parser = ArgParseSettings(description="tierpsy-cli: command-line interface for Tierpsy Tracker")
    @add_arg_table! parser begin
        "--frame-rate"
            help = "Frame rate in frames per second (see also --expected-fps)"
            arg_type = Float64
        "--pixel-size"
            help = "Pixel size in mm (see also --microns-per-pixel)"
            arg_type = Float64
        "--date-time"
            help = "Assay ID typically as yyyymmdd_HHMMSS"
        "--engine"
            help = "Container engine"
            default = "docker"
        "--local-image"
            help = "Tag of the local Docker/Podman image"
        "--mask-min-area"
            arg_type = Int
        "--mask-max-area"
            arg_type = Int
        "--thresh-C"
            arg_type = Int
        "--thresh-block-size"
            arg_type = Int
        "--dilation-size"
            arg_type = Int
        "--save-full-interval"
            action = :store_true
        "--compression-buff"
            arg_type = Int
        "--keep-border-data"
            action = :store_true
        "--background"
            arg_type = Symbol
            default = :light
        "--extract-timestamp"
            action = :store_true
        "--expected-fps"
            arg_type = Float64
        "--microns-per-pixel"
            arg_type = Float64
        "--mask-bgnd-buff-size"
            arg_type = Int
        "--mask-bgnd-frame-gap"
            arg_type = Int
        "--full-bgnd-subtraction"
            action = :store_true
        "--worm-bw-thresh-factor"
            arg_type = Float64
        "--strel-size"
            arg_type = Int
        "--traj-min-area"
            arg_type = Int
        "--traj-min-box-width"
            arg_type = Int
        "--traj-max-allowed-dist"
            arg_type = Int
        "--traj-max-frames-gap"
            arg_type = Int
        "--traj-area-ratio-lim"
            arg_type = Float64
        "--roi-size"
            arg_type = Int
        "--w-num-segments"
            arg_type = Int
        "--w-head-angle-thresh"
            arg_type = Float64
        "--resampling-N"
            arg_type = Int
        "--max-gap-allowed-block"
            arg_type = Int
        "--ht-orient-segment"
            arg_type = Int
        "--filt-bad-seg-thresh"
            arg_type = Float64
        "--filt-max-width-ratio"
            arg_type = Float64
        "--filt-max-area-ratio"
            arg_type = Float64
        "--filt-min-displacement"
            arg_type = Float64
        "--filt-critical-alpha"
            arg_type = Float64
        "input-path"
            help = "Path to movie file"
            required = true
        "output-path"
            help = "Path to output directory"
            required = true
    end
    parsed_args = parse_args(args, parser)
    inputpath = pop!(parsed_args, "input-path")
    outputdir = pop!(parsed_args, "output-path")
    localimage = pop!(parsed_args, "local-image")
    framerate = pop!(parsed_args, "frame-rate")
    pixelsize = pop!(parsed_args, "pixel-size")
    datetime = pop!(parsed_args, "date-time")
    background = pop!(parsed_args, "background")
    if background ∉ (:light, :dark)
        @error "--background can be any of: light, dark" background
        exit()
    end
    function asopt(opt)
        opt = replace(opt, "-"=>"_")
        if opt in ("extract_timestamp", "full_bgnd_subtraction")
            opt = "is_$opt"
        end
        return Symbol(opt)
    end
    isdefault(val) = false
    isdefault(::Nothing) = true
    isdefault(flag::Bool) = !flag
    parameters = OrderedDict{Symbol, Any}(
        asopt(opt)=>val for (opt, val) in pairs(parsed_args) if !isdefault(val)
    )
    parameters[:is_light_background] = background === :light
    if isnothing(parsed_args["expected-fps"]) && !isnothing(framerate)
        parameters[:expected_fps] = framerate
    end
    if isnothing(parsed_args["microns-per-pixel"]) && !isnothing(pixelsize)
        parameters[:microns_per_pixel] = 1e3pixelsize
    end
    runimage(inputpath, outputdir;
        localimage=localimage,
        parameters...)
    if !isnothing(datetime)
        if isnothing(framerate)
            try
                framerate = parameters[:expected_fps]
            catch
                @error "--frame-rate or --expected-fps required for export"
                exit()
            end
        end
        skeletonfile = splitext(basename(inputpath))[1] * "_skeletons.hdf5"
        exportskeletons(datetime, framerate, pixelsize, joinpath(outputdir, skeletonfile))
    end
end

if abspath(PROGRAM_FILE) == @__FILE__
    main()
end

