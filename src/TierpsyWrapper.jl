module TierpsyWrapper

using JSON3
using OrderedCollections
using HDF5

export runimage, exportskeletons, runviewer

const tierpsy_default_image = "tierpsy/tierpsy-tracker:1.5.3a_18aaba9"

const defaultsteps = [
    "COMPRESS",
    "TRAJ_CREATE",
    "TRAJ_JOIN",
    "SKE_INIT",
    "BLOB_FEATS",
    "SKE_CREATE",
    "SKE_FILT",
    "SKE_ORIENT",
]

function buildimage(image::AbstractString; engine::AbstractString="docker")
    projectroot = dirname(Base.active_project())
    containerfile = joinpath(projectroot, "recipe", "Containerfile")
    run(`$engine build --no-cache -f $containerfile -t $image`)
end

function runimage(inputfile, outputdir;
    engine::AbstractString="docker",
    localimage::Union{Nothing,<:AbstractString}=nothing,
    analysis_checkpoints=defaultsteps,
    parameters...
)
    paramfile = "parameters.json"
    mkpath(outputdir)
    open(joinpath(outputdir, paramfile), "w") do f
        writeparameters(f; parameters...)
    end
    joinpath′ = if Sys.iswindows()
        if startswith(inputfile, "~")
            inputfile = joinpath(homedir(), splitpath(inputfile)[2:end]...)
        end
        joinpath′(parts...) = join(parts, "/")
    else
        joinpath
    end
    inputfile = realpath(inputfile)
    outputdir = realpath(outputdir)
    workdir = "/data"
    rawdir = "raw"
    processeddir = "processed"
    mountraw = join((dirname(inputfile), joinpath′(workdir, rawdir)), ":")
    mountprocessed = join((outputdir, joinpath′(workdir, processeddir)), ":")
    cmd = ["/usr/local/bin/python3",
        "/tierpsy-tracker/tierpsy/processing/ProcessLocal.py",
        joinpath′(rawdir, basename(inputfile)),
        "--masks_dir", processeddir,
        "--results_dir", processeddir,
        "--tmp_mask_dir", "/home/tierpsy_user/Tmp/MaskedVideos/",
        "--tmp_results_dir", "/home/tierpsy_user/Tmp/Results/",
        "--json_file", joinpath′(processeddir, paramfile),
        "--analysis_checkpoints", analysis_checkpoints...
    ]
    image = tierpsy_default_image
    extraargs = String[]
    if !isnothing(localimage)
        image = localimage
        if isempty(readchomp(`$engine images -q $image`))
            buildimage(image; engine=engine)
        end
    end
    if occursin("podman", readchomp(`$engine --version`))
        extraargs = [
            "--security-opt", "label=disable",
            "--userns", "keep-id:uid=1000,gid=1000",
        ]
    end
    run(`$engine run --rm -w $workdir $extraargs -v $mountraw -v $mountprocessed $image $cmd`)
end

function writeparameters(f; kwargs...)
    params = OrderedDict{Symbol,Any}(kwargs...)
    get!(params, :analysis_type, "BASE")
    get!(params, :analysis_checkpoints, String[])
    get!(params, :mask_min_area, 30)
    get!(params, :mask_max_area, 10000)
    get!(params, :thresh_C, 15)
    get!(params, :thresh_block_size, 61)
    get!(params, :dilation_size, 9)
    get!(params, :save_full_interval, -1)
    get!(params, :compression_buff, -1)
    get!(params, :keep_border_data, false)
    get!(params, :is_light_background, true)
    get!(params, :is_extract_timestamp, false)
    get!(params, :expected_fps, 30.0)
    get!(params, :microns_per_pixel, 73.0)
    get!(params, :mask_bgnd_buff_size, 100)
    get!(params, :mask_bgnd_frame_gap, 10)
    get!(params, :is_full_bgnd_subtraction, false)
    get!(params, :worm_bw_thresh_factor, 1.05)
    get!(params, :strel_size, 5)
    get!(params, :traj_min_area, 25)
    get!(params, :traj_min_box_width, 5)
    get!(params, :traj_max_allowed_dist, 25)
    get!(params, :traj_max_frames_gap, 0)
    get!(params, :traj_area_ratio_lim, 2)
    get!(params, :roi_size, -1)
    get!(params, :w_num_segments, 20)
    get!(params, :w_head_angle_thresh, 60)
    get!(params, :resampling_N, 11)
    get!(params, :max_gap_allowed_block, -1)
    get!(params, :ht_orient_segment, -1)
    get!(params, :filt_bad_seg_thresh, 0.8)
    get!(params, :filt_max_width_ratio, 2.25)
    get!(params, :filt_max_area_ratio, 6)
    get!(params, :filt_min_displacement, 10)
    get!(params, :filt_critical_alpha, 0.01)
    get!(params, :int_save_maps, false)
    get!(params, :int_avg_width_frac, 0.3)
    get!(params, :int_width_resampling, 15)
    get!(params, :int_length_resampling, 131)
    get!(params, :int_max_gap_allowed_block, -1)
    get!(params, :head_tail_int_method, "MEDIAN_INT")
    get!(params, :split_traj_time, 90)
    get!(params, :ventral_side, "")
    get!(params, :feat_skel_smooth_window, 5)
    get!(params, :feat_coords_smooth_window_s, 0.25)
    get!(params, :feat_gap_to_interp_s, 0.25)
    get!(params, :feat_derivate_delta_time, 0.33)
    get!(params, :n_cores_used, 1)
    get!(params, :nn_filter_to_use, "none") # essential
    # probably of impactless
    get!(params, :path_to_custom_pytorch_model, "")
    get!(params, :use_nn_food_cnt, false)
    get!(params, :MWP_total_n_wells, -1)
    get!(params, :MWP_whichsideup, "upright")
    get!(params, :MWP_well_shape, "square")
    get!(params, :MWP_well_masked_edge, 0.1)
    JSON3.pretty(f, JSON3.write(params))
end

function exportskeletons(runid, framerate, pixelsize, skeletonfile, outputbasename=nothing)
    if isnothing(outputbasename)
        @assert endswith(skeletonfile, "_skeletons.hdf5")
        outputbasename = skeletonfile[1:end-length("_skeletons.hdf5")]
    end
    larvaid_maxlength = 5
    haswarned = false
    open(outputbasename * ".spine", "w") do spinefile
        open(outputbasename * ".outline", "w") do outlinefile
            h5open(skeletonfile, "r") do f
                # TODO: check the unit of the coordinates
                spines = read(f, "skeleton")# * pixelsize
                contour1 = read(f, "contour_side1")
                contour1 = contour1[:, end:-1:1, :]# * pixelsize
                contour2 = read(f, "contour_side2")
                contour2 = contour2[:, 2:end-1, :]# * pixelsize
                for trajectory in read(f, "trajectories_data")
                    if convert(Bool, trajectory.has_skeleton)
                        frameindex = trajectory.frame_number
                        larvaindex = trajectory.worm_index_joined
                        skeletonid = trajectory.skeleton_id + 1
                        spine = spines[:, :, skeletonid]
                        larvaid = lpad(larvaindex, larvaid_maxlength, "0")
                        if !haswarned && larvaid_maxlength < length(larvaid)
                            @warn "Larva id exceeds the maximum expected length" larvaid_maxlength length(larvaid)
                            haswarned = true
                        end
                        timestamp = round(frameindex / framerate; digits=3)
                        writerow(spinefile, runid, larvaid, timestamp, spine)
                        outline = [contour1[:, :, skeletonid] contour2[:, :, skeletonid]]
                        writerow(outlinefile, runid, larvaid, timestamp, outline)
                    end
                end
            end
        end
    end
end

function writerow(f, runid, larvaid, timestamp, coords)
    write(f, "$runid $larvaid $timestamp")
    foreach(coords[:]) do coord
        write(f, " $(round(coord; digits=3))")
    end
    write(f, "\n")
end

function runviewer(path; localimage=nothing, engine=nothing)
    datadir = datafile = nothing # set scope
    if isdir(path)
        datadir = path
        hdf5files = [file for file in readdir(datadir; join=false, sort=false)
            if endswith(file, ".hdf5")]
        datafile = first(sort(hdf5files; by=length))
    else
        datadir = dirname(path)
        datafile = basename(path)
        if !endswith(datafile, ".hdf5")
            datafile = splitext(datafile)[1] * ".hdf5"
            isfile(datafile) || throw("no corresponding .hdf5 files found")
        end
    end
    datadir = realpath(datadir)
    image = tierpsy_default_image
    if !isnothing(localimage)
        image = localimage
        if isempty(readchomp(`$engine images -q $image`))
            buildimage(image; engine=engine)
        end
    end
    xdgdir = ENV["XDG_RUNTIME_DIR"]
    extraargs = ["-e", "DISPLAY=$(ENV["DISPLAY"])",
        "-v", "$xdgdir:$xdgdir",
        "-v", "/tmp/.X11-unix:/tmp/.X11-unix",
        "--hostname", "tierpsydocker",
    ]
    if occursin("podman", readchomp(`$engine --version`))
        extraargs = vcat(extraargs, [
            "--security-opt", "label=disable",
            "--userns", "keep-id:uid=1000,gid=1000",
        ])
    end
    cmd = ["/usr/local/bin/python3", "-c",
    "import sys; from tierpsy.gui.MWTrackerViewer import MWTrackerViewer_GUI; from PyQt5.QtWidgets import QApplication; app = QApplication(sys.argv); main = MWTrackerViewer_GUI(); main.show(); main.updateVideoFile(\"$datafile\"); sys.exit(app.exec_())"]
    run(`$engine run --rm -it -w /data $extraargs -v $datadir:/data $image $cmd`)
end

end
