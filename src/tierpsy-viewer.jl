
using ArgParse

include("TierpsyWrapper.jl")
using .TierpsyWrapper

function main(args=ARGS)
    parser = ArgParseSettings(description="tierpsy-viewer: open a file in Tierpsy Tracker's viewer")
    @add_arg_table! parser begin
        "--engine"
            help = "Container engine"
            default = "docker"
        "--local-image"
            help = "Tag of the local Docker/Podman image"
        "input-path"
            help = "Path to file or its parent directory"
            required = true
    end
    parsed_args = parse_args(args, parser)
    inputpath = pop!(parsed_args, "input-path")
    localimage = pop!(parsed_args, "local-image")
    engine = pop!(parsed_args, "engine")
    runviewer(inputpath;
        localimage=localimage,
        engine=engine)
end

if abspath(PROGRAM_FILE) == @__FILE__
    main()
end

