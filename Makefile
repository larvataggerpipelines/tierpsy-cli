.PHONY: default clean

default: Manifest.toml

Manifest.toml:
	julia --project=. -e 'using Pkg; Pkg.instantiate()'

clean:
	$(RM) Manifest.toml
