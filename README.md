# CLI for Tierpsy Tracker

Command-line interface for [Tierpsy Tracker](https://mgh17.github.io/tierpsy-tracker/), with a focus on *Drosophila* larvae.

## Installation

The general installation procedure is as follows:
```
git clone https://gitlab.com/larvataggerpipelines/tierpsy-cli
cd tierpsy-cli
make
```

On Windows, or if you do not have `make` installed, type instead: `julia --project=. -e 'using Pkg; Pkg.instantiate()'`.

[Julia](https://julialang.org/downloads/) is required, as well as a container engine such as [Docker](https://www.docker.com/products/docker-desktop/) or Podman.

## Usage

The commands below are to be run from the *tierpsy-cli* root directory.

See `julia --project=. src/tierpsy-cli.jl --help`.

Example:
```
julia --project=. src/tierpsy-cli.jl ~/Downloads/A1_2022-07-12-115823-0000_ffv1.avi ./data --frame-rate=30 --pixel-size=0.073 --date-time=20220712_115823
```
All equal signs can be replaced by spaces.

Documentation for most options can be found [here](https://github.com/Tierpsy/tierpsy-tracker/blob/ca1a4c5eb6e6a373721165af643dffdad2a693e8/tierpsy/helper/params/docs_tracker_param.py#L13-L345).

Many default values are overridden, though. In particuliar, `resampling_N` (option `--resampling-N`) is set to 11, to generate 11-point spines and 20-point outlines.
The default sequence of analysis checkpoints stops with the `SKE_ORIENT` checkpoint; intensity-based orientation and feature extraction are disregarded by default.

To control `is_light_background`, pass option `--background=light` or `--background=dark`.

If argument `--date-time` is provided, skeletons are exported to Choreography-like *.spine*-*.outline* ascii files.

If segmentation arguments such as `mask_bgnd_buff_size`, `mask_bgnd_frame_gap`, `thresh_C`, `thresh_block_size` and `dilation_size` do not vary, while trying different values for other parameters, use the masked video as input file (to be copied in the output directory).
The masked video is the *.hdf5* file with no added suffixes in the name.

